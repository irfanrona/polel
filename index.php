<!DOCTYPE html>
<?php

// $data = file_get_contents('http://localhost/api/comment');
// $data = file_get_contents('http://api.irona.online/comment');
$data = file_get_contents('https://api.irona.online/comment');

if ($data) {
  $comments = json_decode($data, true);
}

// var_dump($comments);
// foreach ($comments as $c) {
//   echo $c['name'];
// }
// return 0;
?>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="follow, noindex">
  <meta name="description" content="Minggu, 15 Mei 2022">
  <meta property="og:site_name" content="Undangan Pernikahan Untukmu">
  <meta property="og:title" content="Pernikahan Astri &amp; Irfan">
  <meta property="og:description" content="Minggu, 15 Mei 2022">
  <meta property="og:url" content="https://irona.online/">
  <meta property="og:image" content="http://irona.online/upload/img/landing.jpg">
  <meta property="og:image:secure_url" content="https://irona.online/upload/img/landing.jpg">
  <meta property="og:image:height" content="250">
  <meta property="og:image:width" content="250">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:type" content="article">
  <meta property="og:locale" content="en_US">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Kami Astri &amp; Irfan Menikah !!">
  <meta name="twitter:description" content="Minggu, 15 Mei 2022">
  <meta name="twitter:image" content="https://irona.online/upload/img/landing.jpg">
  <meta name="twitter:label1" content="Ditulis oleh">
  <meta name="twitter:data1" content="Astri | Irfan">

  <link rel="alternate" type="application/rss+xml" title="Undangan Pernikahan Untukmu » Feed" href="https://irona.online/feed">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="theme-color" content="#0134d4">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title>Pernikahan Astri &amp; Irfan</title>
  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Parisienne&display=swap" rel="stylesheet">
  <link href="https://fonts.cdnfonts.com/css/brittany-signature" rel="stylesheet">
  <!-- Favicon -->
  <link rel="icon" type="image/svg+xml" href="data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 width=%22256%22 height=%22256%22 viewBox=%220 0 100 100%22><rect width=%22100%22 height=%22100%22 rx=%2250%22 fill=%22%234a90e2%22></rect><path d=%22M49.86 70.55L48.18 64.88L34.46 64.88Q33.69 67.39 33.02 69.84Q32.36 72.30 31.87 74.19L31.87 74.19Q30.96 74.47 29.98 74.67Q29 74.89 27.60 74.89L27.60 74.89Q24.52 74.89 22.94 73.80Q21.37 72.72 21.37 70.69L21.37 70.69Q21.37 69.70 21.65 68.80Q21.93 67.89 22.28 66.70L22.28 66.70Q22.77 64.81 23.68 62.01Q24.59 59.20 25.67 55.95Q26.76 52.70 27.95 49.27Q29.14 45.84 30.26 42.72Q31.38 39.60 32.29 37.02Q33.20 34.42 33.83 32.88L33.83 32.88Q34.88 32.25 37.09 31.70Q39.29 31.13 41.46 31.13L41.46 31.13Q44.61 31.13 47.02 32.01Q49.44 32.88 50.14 34.91L50.14 34.91Q51.47 38.63 53.01 43.52Q54.55 48.42 56.13 53.61Q57.70 58.78 59.17 63.83Q60.64 68.87 61.76 72.86L61.76 72.86Q60.99 73.77 59.48 74.33Q57.98 74.89 55.88 74.89L55.88 74.89Q52.87 74.89 51.64 73.84Q50.42 72.78 49.86 70.55L49.86 70.55ZM41.81 40.94L41.39 40.94Q40.41 44.09 38.97 48.11Q37.54 52.14 36.28 56.34L36.28 56.34L46.29 56.34Q45.10 51.92 43.88 47.90Q42.65 43.88 41.81 40.94L41.81 40.94ZM78 45.34L78 74.12Q77.30 74.33 76.04 74.50Q74.78 74.67 73.24 74.67L73.24 74.67Q70.09 74.67 68.73 73.66Q67.36 72.64 67.36 69.70L67.36 69.70L67.36 40.94Q68.06 40.73 69.32 40.52Q70.58 40.30 72.12 40.30L72.12 40.30Q75.27 40.30 76.64 41.35Q78 42.41 78 45.34L78 45.34ZM66.73 30.85L66.73 30.85Q66.73 28.48 68.38 26.79Q70.02 25.11 72.68 25.11L72.68 25.11Q75.34 25.11 76.98 26.79Q78.63 28.48 78.63 30.85L78.63 30.85Q78.63 33.30 76.98 34.98Q75.34 36.66 72.68 36.66L72.68 36.66Q70.02 36.66 68.38 34.98Q66.73 33.30 66.73 30.85Z%22 fill=%22%23fff%22></path></svg>" />
  <link rel="apple-touch-icon" href="img/icons/icon-96x96.png">
  <link rel="apple-touch-icon" sizes="152x152" href="img/icons/icon-152x152.png">
  <link rel="apple-touch-icon" sizes="167x167" href="img/icons/icon-167x167.png">
  <link rel="apple-touch-icon" sizes="180x180" href="img/icons/icon-180x180.png">
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-icons.css">
  <link rel="stylesheet" href="css/tiny-slider.css">
  <link rel="stylesheet" href="css/baguetteBox.min.css">
  <link rel="stylesheet" href="css/rangeslider.css">
  <link rel="stylesheet" href="css/vanilla-dataTables.min.css">
  <link rel="stylesheet" href="css/apexcharts.css">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="wedding.css">
  <!-- Web App Manifest -->
  <link rel="manifest" href="manifest.json">
</head>

<body>
  <!-- Preloader -->
  <div id="preloader">
    <div class="spinner-grow text-primary" role="status"><span class="visually-hidden">Memuat...</span></div>
  </div>
  <!-- Internet Connection Status -->
  <!-- # This code for showing internet connection status -->
  <div class="internet-connection-status" id="internetStatus"></div>
  <!-- Dark mode switching -->
  <div class="dark-mode-switching">
    <div class="d-flex w-100 h-100 align-items-center justify-content-center">
      <div class="dark-mode-text text-center">
        <svg class="bi bi-moon" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M14.53 10.53a7 7 0 0 1-9.058-9.058A7.003 7.003 0 0 0 8 15a7.002 7.002 0 0 0 6.53-4.47z"></path>
        </svg>
        <p class="mb-0">Switching to dark mode</p>
      </div>
      <div class="light-mode-text text-center">
        <svg class="bi bi-brightness-high" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" viewBox="0 0 16 16">
          <path d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8zM8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0zm0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13zm8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0zm-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zm9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707zM4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708z"></path>
        </svg>
        <p class="mb-0">Switching to light mode</p>
      </div>
    </div>
  </div>
  <!-- Setting Popup Overlay -->
  <div id="setting-popup-overlay"></div>
  <!-- Setting Popup Card -->
  <div class="card setting-popup-card shadow-lg" id="settingCard">
    <div class="card-body">
      <div class="container">
        <div class="setting-heading d-flex align-items-center justify-content-between mb-3">
          <p class="mb-0">Settings</p>
          <div class="btn-close" id="settingCardClose"></div>
        </div>
        <div class="single-setting-panel">
          <div class="form-check form-switch mb-2">
            <input class="form-check-input" type="checkbox" id="darkSwitch">
            <label class="form-check-label" for="darkSwitch">Dark mode</label>
          </div>
          <div class="form-check form-switch mb-2">
            <input class="form-check-input" type="checkbox" id="soundSwitch" onClick="togglePlay()" checked>
            <label class="form-check-label" for="soundSwitch">Play Song</label>
            <audio id="background_music" loop="loop" src="https://api.our-wedding.link/uploads/4c9081c0-ada5-11eb-a4c4-2be7f1948457.mp3" data-v-2da7ac3a=""></audio>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Header Area -->
  <div class="header-area" id="headerArea">
    <div class="container">
      <!-- # Paste your Header Content from here -->
      <!-- # Header Five Layout -->
      <!-- # Copy the code from here ... -->
      <!-- Header Content -->
      <div class="header-content header-style-five position-relative d-flex align-items-center justify-content-between">
        <!-- Logo Wrapper -->
        <div class="logo-wrapper">Astri <svg class="bi bi-hearts text-secondary" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M4.931.481c1.627-1.671 5.692 1.254 0 5.015-5.692-3.76-1.626-6.686 0-5.015Zm6.84 1.794c1.084-1.114 3.795.836 0 3.343-3.795-2.507-1.084-4.457 0-3.343ZM7.84 7.642c2.71-2.786 9.486 2.09 0 8.358-9.487-6.268-2.71-11.144 0-8.358Z" />
          </svg> Irfan</div>
        <!-- Navbar Toggler -->
        <!-- Settings -->
        <div class="setting-wrapper">
          <div class="setting-trigger-btn" id="settingTriggerBtn">
            <svg class="bi bi-gear" width="18" height="18" viewBox="0 0 16 16" fill="url(#gradientSettings)" xmlns="http://www.w3.org/2000/svg">
              <defs>
                <linearGradient id="gradientSettings" spreadMethod="pad" x1="0%" y1="0%" x2="100%" y2="100%">
                  <stop offset="0" style="stop-color: #c57fe8; stop-opacity:1;"></stop>
                  <stop offset="100%" style="stop-color: #4eace1; stop-opacity:1;"></stop>
                </linearGradient>
              </defs>
              <path fill-rule="evenodd" d="M8.837 1.626c-.246-.835-1.428-.835-1.674 0l-.094.319A1.873 1.873 0 0 1 4.377 3.06l-.292-.16c-.764-.415-1.6.42-1.184 1.185l.159.292a1.873 1.873 0 0 1-1.115 2.692l-.319.094c-.835.246-.835 1.428 0 1.674l.319.094a1.873 1.873 0 0 1 1.115 2.693l-.16.291c-.415.764.42 1.6 1.185 1.184l.292-.159a1.873 1.873 0 0 1 2.692 1.116l.094.318c.246.835 1.428.835 1.674 0l.094-.319a1.873 1.873 0 0 1 2.693-1.115l.291.16c.764.415 1.6-.42 1.184-1.185l-.159-.291a1.873 1.873 0 0 1 1.116-2.693l.318-.094c.835-.246.835-1.428 0-1.674l-.319-.094a1.873 1.873 0 0 1-1.115-2.692l.16-.292c.415-.764-.42-1.6-1.185-1.184l-.291.159A1.873 1.873 0 0 1 8.93 1.945l-.094-.319zm-2.633-.283c.527-1.79 3.065-1.79 3.592 0l.094.319a.873.873 0 0 0 1.255.52l.292-.16c1.64-.892 3.434.901 2.54 2.541l-.159.292a.873.873 0 0 0 .52 1.255l.319.094c1.79.527 1.79 3.065 0 3.592l-.319.094a.873.873 0 0 0-.52 1.255l.16.292c.893 1.64-.902 3.434-2.541 2.54l-.292-.159a.873.873 0 0 0-1.255.52l-.094.319c-.527 1.79-3.065 1.79-3.592 0l-.094-.319a.873.873 0 0 0-1.255-.52l-.292.16c-1.64.893-3.433-.902-2.54-2.541l.159-.292a.873.873 0 0 0-.52-1.255l-.319-.094c-1.79-.527-1.79-3.065 0-3.592l.319-.094a.873.873 0 0 0 .52-1.255l-.16-.292c-.892-1.64.902-3.433 2.541-2.54l.292.159a.873.873 0 0 0 1.255-.52l.094-.319z"></path>
              <path fill-rule="evenodd" d="M8 5.754a2.246 2.246 0 1 0 0 4.492 2.246 2.246 0 0 0 0-4.492zM4.754 8a3.246 3.246 0 1 1 6.492 0 3.246 3.246 0 0 1-6.492 0z"></path>
            </svg><span></span>
          </div>
        </div>
      </div>
      <!-- # Header Five Layout End -->
    </div>
  </div>
  <!-- # Sidenav Left -->
  <div class="page-content-wrapper r-to-top" id="Beranda">
    <!-- Welcome Toast -->
    <div class="toast toast-autohide custom-toast-1 toast-warning home-page-toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="7000" data-bs-autohide="true">
      <div class="toast-body">
        <svg class="bi bi-bookmark-check text-white" width="30" height="30" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"></path>
          <path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"></path>
        </svg>
        <div class="toast-text ms-3 me-2">
          <p class="mb-1 text-white">Undangan Pernikahan!</p><small class="d-block">Kami mengundang anda pada acara pernikahan kami, dimohon tamu undangan mematuhi protokol kesehatan.</small>
        </div>
      </div>
      <button class="btn btn-close btn-close-white position-absolute p-1" type="button" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <!-- CountDown -->
    <div class="coming-soon-wrapper bg-white text-center bg-overlay" style="background-image: url('upload/img/banner_1.jpg')">
      <div class="container">
        <h2 class="text-white display-3">Astri
          <svg class="bi bi-heart text-danger" width="20" height="20" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"></path>
          </svg>
          Irfan
        </h2>
        <p class="text-white-50 fw-lighter fst-italic display-3">Kami telah menikah!
        <!-- <p class="text-white">Akan segera melangsungkan pernikahan. -->
        <blockquote>Minggu, 15 Mei 2022</blockquote>
        </p>
        <!-- hapus EXPIRED di id contdown2 -->
        <div class="countdown2 justify-content-center" id="countdown2EXPIRED" data-date="EXPIRED" data-time="09:00">
          <div class="day"><span class="num"></span><span class="word"></span></div>
          <div class="hour"><span class="num"></span><span class="word"></span></div>
          <div class="min"><span class="num"></span><span class="word"></span></div>
          <div class="sec"><span class="num"></span><span class="word"></span></div>
        </div>
        <div class="notify-email mt-5 r-index-3">
          <button class="btn btn-warning r-baca-protokol" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Baca Protokol</button>
        </div>
        <!-- Particles -->
        <div class="r-background" id="particles-js"></div>
      </div>
    </div>
    <div class="pt-3"></div>
    <div class="pt-3"></div>
    <div class="pt-3"></div>
    <!-- Quote -->
    <div class="container direction-rtl r-to-id" id="Mempelai">
      <h1 class="ps-1 text-center card-title r-font-parisienne">Bismillahirrahmanirrahim</h1>
      <div class="pt-3"></div>
      <h5 class="text-center">وَ مِنۡ اٰیٰتِہٖۤ اَنۡ خَلَقَ لَکُمۡ مِّنۡ اَنۡفُسِکُمۡ اَزۡوَاجًا لِّتَسۡکُنُوۡۤا اِلَیۡہَا وَ جَعَلَ بَیۡنَکُمۡ مَّوَدَّۃً وَّ رَحۡمَۃً ؕ اِنَّ فِیۡ ذٰلِکَ لَاٰیٰتٍ لِّقَوۡمٍ یَّتَفَکَّرُوۡنَ</h5>
      <div class="pt-3"></div>
      <figure class="text-center">
        <blockquote class="blockquote">
          <p><small>Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu istri-istri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya di antaramu rasa kasih dan sayang, sesungguhnya pada yang demikian itu benar benar terdapat tanda-tanda bagi kaum yang berpikir.</small></p>
        </blockquote>
        <figcaption class="blockquote-footer">
          QS. Ar-Rum <cite title="Source Title">30 : 21</cite>
        </figcaption>
      </figure>
    </div>
    <div class="pt-3"></div>
    <!-- Mempelai -->
    <div class="container">
      <div class="card">
        <div class="element-heading card-body">
          <h4 class="ps-1 text-center card-title">Pasangan Mempelai</h4>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="card-body align-items-center direction-rtl text-center">
              <div class="card-content">
                <img class="img-circle w-200" src="upload/img/ava_bride.png" alt="">
                <h5 class="mb-3 mt-2">Astri Artiningsih, S.Pd.</h5>
                <p>Putri tercinta dari <br> Bapak<span class="m-1 badge rounded-pill bg-secondary">Sudjatmika, S.H.</span> dan Ibu<span class="m-1 badge rounded-pill bg-secondary">Yeti Kusniati</span></p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-body align-items-center direction-rtl text-center">
              <div class="card-content">
                <img class="img-circle w-200" src="upload/img/ava_grom.png" alt="">
                <h5 class="mb-3 mt-2">Irfan Rona, S.Pd.</h5>
                <p>Putra tercinta dari <br> Bapak<span class="m-1 badge rounded-pill bg-secondary">H. Edi Setiadi, S.T.</span>dan Ibu<span class="m-1 badge rounded-pill bg-secondary">Hj. Siti Chomsah Fatimah</span></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pt-3"></div>
    <!-- Acara -->
    <div class="container direction-rtl r-to-id" id="Acara">
      <div class="card">
        <div class="card-body">
          <div class="order-done-content text-center">
            <div>
              <h4 class="ps-1 text-center card-title">Minggu, 15 Mei 2022</h4>
              <div class="row mt-4 ">
                <div class="col-md-6 mb-4">
                  <h3 class="fw-light">Akad nikah</h3>
                  <div class="progress mb-4" style="height: 0.1rem;">
                    <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="50" aria-valuemax="100" style="width: 100%"></div>
                  </div>
                  <p><strong class="text-dark shadow-sm me-2 fz-14"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-watch" viewBox="0 0 16 16">
                        <path d="M8.5 5a.5.5 0 0 0-1 0v2.5H6a.5.5 0 0 0 0 1h2a.5.5 0 0 0 .5-.5V5z" />
                        <path d="M5.667 16C4.747 16 4 15.254 4 14.333v-1.86A5.985 5.985 0 0 1 2 8c0-1.777.772-3.374 2-4.472V1.667C4 .747 4.746 0 5.667 0h4.666C11.253 0 12 .746 12 1.667v1.86a5.99 5.99 0 0 1 1.918 3.48.502.502 0 0 1 .582.493v1a.5.5 0 0 1-.582.493A5.99 5.99 0 0 1 12 12.473v1.86c0 .92-.746 1.667-1.667 1.667H5.667zM13 8A5 5 0 1 0 3 8a5 5 0 0 0 10 0z" />
                      </svg></strong>Pukul 09:00 WIB - Selesai
                  </p>
                </div>
                <div class="col-md-6 mb-4">
                  <h3 class="fw-light">Resepsi</h3>
                  <div class="progress mb-4" style="height: 0.1rem;">
                    <div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="50" aria-valuemax="100" style="width: 100%"></div>
                  </div>
                  <p><strong class="text-dark shadow-sm me-2 fz-14"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-watch" viewBox="0 0 16 16">
                        <path d="M8.5 5a.5.5 0 0 0-1 0v2.5H6a.5.5 0 0 0 0 1h2a.5.5 0 0 0 .5-.5V5z" />
                        <path d="M5.667 16C4.747 16 4 15.254 4 14.333v-1.86A5.985 5.985 0 0 1 2 8c0-1.777.772-3.374 2-4.472V1.667C4 .747 4.746 0 5.667 0h4.666C11.253 0 12 .746 12 1.667v1.86a5.99 5.99 0 0 1 1.918 3.48.502.502 0 0 1 .582.493v1a.5.5 0 0 1-.582.493A5.99 5.99 0 0 1 12 12.473v1.86c0 .92-.746 1.667-1.667 1.667H5.667zM13 8A5 5 0 1 0 3 8a5 5 0 0 0 10 0z" />
                      </svg></strong>Pukul 11:00 WIB - 14:00 WIB
                  </p>
                </div>
              </div>
              <div class="fs-4 mb-1">
                <a class="btn btn-lg m-1 btn-round btn-outline-info" href="https://goo.gl/maps/zFTzDXy1y3Fhid6t9" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                    <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z" />
                    <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                  </svg>
                  Hotel Mandiri
                </a>
              </div>
              <p>Seluruh acara bertempat di Ballroom Hotel Mandiri. <br> Di Jalan RE. Kosasih, Parunglesang, Kec. Banjar, Kota Banjar.</p>
              <a class="btn btn-warning mt-3" href="https://calendar.google.com/event?action=TEMPLATE&tmeid=M2M5ajlmcjJhZHRoa3VwbGJiZWJtNWp1azQgaXJmYW4ucm9uYTk1QHN0dWRlbnQudXBpLmVkdQ&tmsrc=irfan.rona95%40student.upi.edu">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-journal-bookmark-fill" viewBox="0 0 16 16">
                  <path fill-rule="evenodd" d="M6 1h6v7a.5.5 0 0 1-.757.429L9 7.083 6.757 8.43A.5.5 0 0 1 6 8V1z" />
                  <path d="M3 0h10a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-1h1v1a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H3a1 1 0 0 0-1 1v1H1V2a2 2 0 0 1 2-2z" />
                  <path d="M1 5v-.5a.5.5 0 0 1 1 0V5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0V8h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1zm0 3v-.5a.5.5 0 0 1 1 0v.5h.5a.5.5 0 0 1 0 1h-2a.5.5 0 0 1 0-1H1z" />
                </svg>
                Ingatkan di kalender
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pb-3"></div>
    <!-- Turut mengundang -->
    <div class="container">
      <div class="card mb-3 bg-img r-bg-right" style="background-image: url('upload/img/ornamen2.png')">
        <div class="card-body">
          <h3>Turut Mengundang</h3>
          <div class="testimonial-slide-three-wrapper">
            <div class="testimonial-slide3 testimonial-style3">
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">H. Jeje Wiradinata</h6><span class="d-block">Bupati, Kabupaten Pangandaran</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Dr. Drs. H. Ade Setiana, M.Pd.</h6><span class="d-block">Sekertaris Daerah, Kota Banjar</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Drs. H. Asep Tatang Iskandar, M.Si.</h6><span class="d-block">Kepala BKPSDM, Kota Banjar</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Drs. H. Kaswad, M.Pd.I.</h6><span class="d-block">Kepala Disdikbud, Kota Banjar</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Doni Agus Maulana, S.Pd.</h6><span class="d-block">Kepala Sekolah, SMK BPI Bandung</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Kusmana, S.Pd. M.Pd.</h6><span class="d-block">Kepala Sekolah, SMPN 6 Banjar</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Nia Kurniasih, S.Pd. M.Pd.</h6><span class="d-block">Kepala Sekolah, SMPN 3 Banjar</span>
                </div>
              </div>
              <!-- Single Testimonial Slide -->
              <div class="single-testimonial-slide">
                <div class="text-content"><span class="d-inline-block badge bg-warning mb-2"><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill me-1"></i><i class="bi bi-star-fill"></i></span>
                  <h6 class="mb-2">Cecep Erliana, SP.</h6><span class="d-block">Ketua RW 06, Cibulan</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Galeri Foto -->
    <div class="container r-to-id" id="Galeri">
      <div class="card">
        <div class="card-body">
          <div class="row gx-2 align-items-end">
            <div class="col-8">
              <div class="image-gallery-text mb-4">
                <h3 class="mb-1">Galeri Berharga</h3>
                <p class="mb-0">Potret kami yang sedang berbahagia, membuat kami tak berhenti bersyukur.</p>
              </div>
            </div>
            <div class="col-4 text-end"><a class="btn btn-primary btn-sm mb-4" href="https://instagram.com/irfanrona">Selengkapnya</a></div>
          </div>
          <!-- Video Prewedding -->
          <div class="ratio ratio-16x9">
            <div id="player"></div>
          </div>
          <div class="pb-3"></div>
          <!-- Image Gallery Slides Wrapper -->
          <div class="image-gallery-slides-wrapper">
            <div class="image-gallery-carousel gallery-img">
              <!-- Gallery Image 1 -->
              <a class="single-gallery-item" href="upload/img/slider/p1.jpg"><img src="upload/img/slider/p1.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 2 -->
              <a class="single-gallery-item" href="upload/img/slider/p2.jpg"><img src="upload/img/slider/p2.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 3 -->
              <a class="single-gallery-item" href="upload/img/slider/p3.jpg"><img src="upload/img/slider/p3.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 4 -->
              <a class="single-gallery-item" href="upload/img/slider/p4.jpg"><img src="upload/img/slider/p4.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 5 -->
              <a class="single-gallery-item" href="upload/img/slider/p5.jpg"><img src="upload/img/slider/p5.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 6 -->
              <a class="single-gallery-item" href="upload/img/slider/p6.jpg"><img src="upload/img/slider/p6.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 7 -->
              <a class="single-gallery-item" href="upload/img/slider/p7.jpg"><img src="upload/img/slider/p7.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 8 -->
              <a class="single-gallery-item" href="upload/img/slider/p8.jpg"><img src="upload/img/slider/p8.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
            </div>
          </div>
          <div class="pb-3"></div>
          <!-- Image Gallery Slides Wrapper -->
          <div class="image-gallery-slides-wrapper2">
            <div class="image-gallery-carousel2 gallery-img">
              <!-- Gallery Image 1 -->
              <a class="single-gallery-item" href="upload/img/slider/l1.jpg"><img src="upload/img/slider/l1.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 2 -->
              <a class="single-gallery-item" href="upload/img/slider/l2.jpg"><img src="upload/img/slider/l2.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 3 -->
              <a class="single-gallery-item" href="upload/img/slider/l3.jpg"><img src="upload/img/slider/l3.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 4 -->
              <a class="single-gallery-item" href="upload/img/slider/l4.jpg"><img src="upload/img/slider/l4.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 5 -->
              <a class="single-gallery-item" href="upload/img/slider/l5.jpg"><img src="upload/img/slider/l5.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
              <!-- Gallery Image 6 -->
              <a class="single-gallery-item" href="upload/img/slider/l6.jpg"><img src="upload/img/slider/l6.jpg" alt="">
                <!-- Fav Icon -->
                <div class="fav-icon shadow"><i class="bi bi-heart-fill"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pb-3"></div>
    <div class="pb-3"></div>
    <!-- Nomor Rekening -->
    <div class="container">
      <div class="element-heading">
        <h6 class="ps-1">Amplop Digital</h6>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-6">
          <div class="card card-bg-img bg-img bg-overlay mb-3" style="background-image: url('upload/img/ava_grom.jpg')">
            <div class="card-body direction-rtl p-5">
              <h2 class="text-white">
                <div class="d-flex justify-content-start mb-1">
                  <div class="">
                    <button class="btn btn-creative btn-light" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="1320021996005">
                      <svg width="60" height="20" fill="currentColor" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" viewBox="0 0 59.999561 17.510006" version="1.1" id="svg945" inkscape:version="0.92.2 (5c3e80d, 2017-08-06)" sodipodi:docname="Bank Mandiri logo 2016.svg">
                        <defs id="defs939">
                          <clipPath clipPathUnits="userSpaceOnUse" id="clipPath20">
                            <path d="M 382.677,748.082 H 552.755 V 799.37 H 382.677 Z" id="path18" inkscape:connector-curvature="0" />
                          </clipPath>
                        </defs>
                        <sodipodi:namedview id="base" pagecolor="#ffffff" bordercolor="#666666" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="0.35" inkscape:cx="140.52785" inkscape:cy="-472.62453" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" fit-margin-top="0" fit-margin-left="0" fit-margin-right="0" fit-margin-bottom="0" inkscape:window-width="1600" inkscape:window-height="837" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" />
                        <metadata id="metadata942">
                          <rdf:RDF>
                            <cc:Work rdf:about="">
                              <dc:format>image/svg+xml</dc:format>
                              <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                              <dc:title></dc:title>
                            </cc:Work>
                          </rdf:RDF>
                        </metadata>
                        <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(60.237875,-6.2747549)">
                          <g transform="matrix(0.35277777,0,0,-0.35277777,-195.23799,287.69146)" id="g14">
                            <g id="g16" clip-path="url(#clipPath20)">
                              <g id="g22" transform="translate(382.8575,763.6027)">
                                <path d="M 0,0 C 0,2.465 -0.047,4.533 -0.18,6.378 H 4.484 L 4.703,3.211 h 0.13 c 1.055,1.674 2.992,3.654 6.598,3.654 2.815,0 5.013,-1.59 5.936,-3.962 h 0.09 c 0.751,1.189 1.629,2.065 2.638,2.683 1.187,0.835 2.552,1.279 4.314,1.279 3.558,0 7.161,-2.42 7.161,-9.285 v -12.619 h -5.276 v 11.831 c 0,3.561 -1.23,5.673 -3.821,5.673 -1.851,0 -3.217,-1.321 -3.785,-2.861 -0.132,-0.527 -0.265,-1.186 -0.265,-1.799 v -12.844 h -5.279 v 12.402 c 0,2.99 -1.186,5.102 -3.692,5.102 C 7.428,2.465 6.067,0.882 5.583,-0.611 5.364,-1.143 5.271,-1.757 5.271,-2.373 V -15.039 H 0 Z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path24" inkscape:connector-curvature="0" />
                              </g>
                              <g id="g26" transform="translate(429.7098,759.028)">
                                <path d="m 0,0 c -3.827,0.09 -7.475,-0.743 -7.475,-4 0,-2.108 1.364,-3.079 3.079,-3.079 2.151,0 3.738,1.407 4.217,2.946 C -0.046,-3.738 0,-3.296 0,-2.946 Z m 5.276,-5.32 c 0,-1.934 0.089,-3.818 0.311,-5.144 H 0.704 L 0.353,-8.088 H 0.216 c -1.317,-1.672 -3.559,-2.858 -6.327,-2.858 -4.312,0 -6.729,3.116 -6.729,6.377 0,5.409 4.79,8.133 12.707,8.09 v 0.352 c 0,1.408 -0.573,3.739 -4.351,3.739 -2.113,0 -4.315,-0.663 -5.763,-1.586 l -1.057,3.517 c 1.583,0.971 4.356,1.897 7.742,1.897 6.861,0 8.838,-4.358 8.838,-9.018 z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path28" inkscape:connector-curvature="0" />
                              </g>
                              <g id="g30" transform="translate(438.7279,763.6027)">
                                <path d="m 0,0 c 0,2.465 -0.049,4.533 -0.177,6.378 h 4.745 l 0.265,-3.21 h 0.13 c 0.923,1.668 3.257,3.697 6.816,3.697 3.742,0 7.611,-2.42 7.611,-9.193 v -12.711 h -5.411 v 12.093 c 0,3.079 -1.144,5.411 -4.089,5.411 C 7.735,2.465 6.243,0.928 5.668,-0.701 5.496,-1.187 5.449,-1.848 5.449,-2.457 V -15.039 H 0 Z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path32" inkscape:connector-curvature="0" />
                              </g>
                              <g id="g34" transform="translate(476.1589,761.0557)">
                                <path d="m 0,0 c 0,0.437 -0.045,0.965 -0.131,1.405 -0.485,2.111 -2.198,3.825 -4.662,3.825 -3.474,0 -5.408,-3.08 -5.408,-7.081 0,-3.914 1.934,-6.776 5.366,-6.776 2.196,0 4.133,1.497 4.659,3.828 C -0.045,-4.314 0,-3.782 0,-3.211 Z M 5.408,17.53 V -6.466 c 0,-2.198 0.09,-4.575 0.176,-6.024 H 0.747 l -0.216,3.383 h -0.09 c -1.275,-2.371 -3.876,-3.867 -6.994,-3.867 -5.1,0 -9.147,4.347 -9.147,10.946 -0.043,7.171 4.444,11.44 9.589,11.44 2.944,0 5.057,-1.233 6.023,-2.822 H 0 v 10.94 z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path36" inkscape:connector-curvature="0" />
                              </g>
                              <path d="m 485.391,769.981 h 5.461 v -21.415 h -5.461 z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path38" inkscape:connector-curvature="0" />
                              <g id="g40" transform="translate(494.6713,763.0753)">
                                <path d="M 0,0 C 0,2.902 -0.044,4.972 -0.177,6.904 H 4.53 L 4.704,2.818 h 0.179 c 1.054,3.034 3.562,4.086 5.849,4.086 0.527,0 0.835,0.091 1.275,0 V 2.153 C 11.567,2.243 11.084,2.331 10.421,2.331 7.829,2.331 6.07,0.661 5.588,-1.757 5.5,-2.24 5.413,-2.816 5.413,-3.434 V -14.51 H 0 Z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path42" inkscape:connector-curvature="0" />
                              </g>
                              <path d="m 509.185,769.981 h 5.45 v -21.415 h -5.45 z" style="fill:#003a70;fill-opacity:1;fill-rule:nonzero;stroke:none" id="path44" inkscape:connector-curvature="0" />
                              <g id="g46" transform="translate(548.2855,792.6043)">
                                <path d="m 0,0 c -2.691,3.065 -5.547,1.681 -7.827,0.551 -0.955,-0.472 -4.422,-2.78 -7.858,-4.389 -2.448,-1.147 -5.962,-0.8 -7.889,1.58 -0.117,0.144 -3.232,3.918 -3.562,4.383 -2.023,2.537 -7.14,4.641 -13.254,1.142 -3.276,-1.896 -10.996,-6.326 -13.882,-7.974 -1.754,-0.895 -5.826,-1.286 -8.126,1.821 -0.038,0.051 -3.06,3.822 -3.181,3.967 -0.089,0.102 -2.041,2.997 -6.392,3.086 -0.64,0.016 -3.836,0.035 -6.959,-1.738 -4.141,-2.369 -13.779,-7.879 -13.779,-7.879 -0.007,0 -0.007,-0.005 -0.007,-0.005 -3.962,-2.268 -7.052,-4.029 -7.052,-4.029 l 3.648,-4.484 c 1.708,-2.117 5.554,-3.758 8.892,-1.842 0,0 12.33,7.14 12.373,7.16 5.334,2.927 9.45,2.927 12.177,1.837 2.453,-1.033 4.585,-3.613 4.585,-3.613 0,0 2.789,-3.456 3.279,-4.06 1.586,-1.952 4.209,-1.186 4.209,-1.186 0,0 0.969,0.112 2.441,0.982 0,0 11.94,6.923 11.947,6.925 3.794,2.225 7.274,2.64 9.049,2.478 5.567,-0.507 7.298,-4.396 9.714,-7.108 1.421,-1.599 2.702,-2.503 4.663,-2.457 1.291,0.029 2.746,0.808 2.961,0.948 l 14.3,8.255 c 0,0 -1.465,2.208 -4.47,5.649" style="fill:#ffb700;fill-opacity:1;fill-rule:evenodd;stroke:none" id="path48" inkscape:connector-curvature="0" />
                              </g>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                </div>
                <span id="mandiri">1320021996005</span>
              </h2>
              <p class="mb-4 text-white"> a/n Irfan Rona</p>
              <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="1320021996005">Salin No. Rekening</button>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-6">
          <div class="card card-bg-img bg-img bg-overlay mb-3" style="background-image: url('upload/img/ava_bride.jpg')">
            <div class="card-body direction-rtl p-5">
              <h2 class="text-white">
                <div class="d-flex justify-content-start mb-1">
                  <div class="">
                    <button class="btn btn-creative btn-light" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="085323681185">
                      <svg xmlns="http://www.w3.org/2000/svg" width="60" height="16" fill="currentColor" viewBox="0 0 77 24">
                        <title>
                          Logo_ovo_purple
                        </title>
                        <path d="M21.19,20.24a12.54,12.54,0,0,1-8.8,3.45,12.59,12.59,0,0,1-8.85-3.45,11.68,11.68,0,0,1,0-16.77A12.55,12.55,0,0,1,12.4,0a12.54,12.54,0,0,1,8.8,3.45,11.67,11.67,0,0,1,0,16.77M12.4,3.86a7.73,7.73,0,0,0-7.8,8,7.78,7.78,0,1,0,15.56,0,7.73,7.73,0,0,0-7.76-8m38-1.07L40.89,24l-.54-.1c-2.72-.49-3.27-1.17-4.54-3.93L28.68,4.44H26V.67H36.13V4.44H33.71l5.73,12.85L45,4.44H42V.67h8.4Zm23,17.45a13,13,0,0,1-17.64,0,11.68,11.68,0,0,1,0-16.77,13,13,0,0,1,17.64,0,11.65,11.65,0,0,1,0,16.77M64.65,3.86a7.74,7.74,0,0,0-7.81,8,7.78,7.78,0,1,0,15.56,0,7.72,7.72,0,0,0-7.75-8" fill="#4b2489" fill-rule="evenodd"></path>
                      </svg></button>
                  </div>
                  <div class="px-2">
                    <button class="btn btn-creative btn-light" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="0275749954">
                      <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="60" height="16" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 645 240.11" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003">
                        <defs>
                          <style type="text/css">
                            .fil1 {
                              fill: #EC6722
                            }

                            .fil0 {
                              fill: #007697
                            }
                          </style>
                        </defs>
                        <g id="Layer_x0020_1">
                          <metadata id="CorelCorpID_0Corel-Layer" />
                          <path class="fil0" d="M221.82 0c7.52,5.15 15.17,12.96 15.17,20.5l0 132.97c0,7.45 -7.03,17.99 -13.42,22.19 22.56,0 45.13,0 67.69,0 43.8,-1.81 70.36,-17.62 73.54,-50.78 2.22,-23.25 -10.06,-41.33 -40.25,-51.53 15.06,-5.73 23.16,-18.21 23.9,-35.42 1.27,-29.6 -33.18,-37.93 -54.85,-37.93l-71.78 0zm42.01 71.2l0 -55.44 20.78 0c23.56,0 34.67,11.56 34.66,28.99 0,17.43 -15.94,26.45 -35.83,26.45l-19.61 0zm22.15 87.53l-22.15 0 0 -71.78 25.1 0c37.38,0 48.69,22.48 46.68,40.85 -2,18.38 -17.79,30.93 -49.63,30.93zm138.42 -158.73l-36.78 0c8.18,9.1 11.74,13.88 11.74,27.99l0 121.18c0,13.95 -6.18,22.52 -15.06,28.58l48.95 0c-7.27,-4.44 -15.09,-13.15 -15.09,-28.91l0 -107.04c24.72,33.02 49.45,66.05 74.17,99.08 14.02,18.34 30.27,33.89 49.22,46.07l0 -159.6c0,-15.98 6.45,-22.08 15.07,-27.35l-47.72 0c8.65,6.42 13.24,13.66 13.24,26.72l0 106.87 -97.74 -133.59zm159.31 177.75c9.9,-6.57 14.93,-14.28 14.93,-24.28l0 -126.93c0,-15 -4.33,-18.4 -12.83,-26.54l56.49 0c-9.75,5.49 -16.59,16.7 -16.59,26.54l0 126.58c0,9.71 5.75,18.61 13.55,24.63l-55.55 0z" />
                          <path class="fil1" d="M0 0l54.53 0c-2.85,4.89 -5,15.88 0.51,40.56 4.9,21.94 19.07,44.82 33.86,62.91l-18.58 15.25 -70.32 -88.35 0 -30.37zm79.63 0l96 0 0 23.26c-7.43,-4.34 -20.97,-8.53 -40.85,-0.58 -32.08,12.84 -34.83,50.42 -34.83,50.42 -17.75,-21.44 -29.42,-56.59 -20.32,-73.1zm96 54.12l0 69.24c-21.81,8.28 -52.41,-13.25 -60.1,-38.81 -11.14,-37.04 15.12,-50.41 33.91,-46.74 14.05,2.74 22.2,11.11 26.19,16.31zm0 80.6l0 40.92 -58.84 0 11.8 -9.69c-7.7,0.94 -22.82,-2.68 -30.18,-11.93l-20.71 -26.02 18.93 -15.55c11.64,12.83 22.72,22.04 28.98,24.81 13.42,5.92 28.53,8.77 50.02,-2.54zm-91.69 40.92l-64.27 0 40.04 -32.88c3.28,4.12 6.44,8.1 9.45,11.87 5.9,7.42 10.85,14.73 14.78,21.01zm-82.95 0l-0.99 0 0 -107.91c15.29,19.22 34.83,43.77 52.33,65.76l-51.34 42.15z" />
                          <path class="fil0" d="M607.72 226.32l0 0 3.3 -0.49c0.19,1.27 0.68,2.24 1.55,3.01 0.87,0.68 2.04,1.07 3.59,1.07 1.46,0 2.63,-0.39 3.4,-0.97 0.68,-0.58 1.07,-1.36 1.07,-2.23 0,-0.68 -0.29,-1.27 -0.97,-1.75 -0.49,-0.29 -1.65,-0.68 -3.4,-1.07 -2.43,-0.68 -4.17,-1.17 -5.05,-1.65 -0.97,-0.49 -1.75,-1.07 -2.23,-1.84 -0.49,-0.78 -0.68,-1.66 -0.68,-2.63 0,-0.87 0.19,-1.74 0.58,-2.42 0.39,-0.78 0.97,-1.36 1.65,-1.85 0.49,-0.39 1.26,-0.78 2.14,-0.97 0.87,-0.29 1.84,-0.48 2.81,-0.48 1.56,0 2.92,0.29 4.08,0.67 1.17,0.49 2.04,1.07 2.62,1.85 0.49,0.78 0.88,1.75 1.17,3.01l-3.3 0.48c-0.2,-1.06 -0.58,-1.84 -1.26,-2.42 -0.68,-0.49 -1.75,-0.78 -3.01,-0.78 -1.56,0 -2.63,0.19 -3.31,0.68 -0.68,0.58 -0.97,1.17 -0.97,1.84 0,0.39 0.2,0.78 0.39,1.17 0.29,0.29 0.68,0.58 1.26,0.87 0.39,0.1 1.36,0.39 3.01,0.88 2.33,0.58 3.98,1.07 4.86,1.55 0.97,0.39 1.75,0.97 2.23,1.75 0.49,0.68 0.78,1.65 0.78,2.82 0,1.06 -0.29,2.13 -0.97,3.1 -0.58,0.97 -1.56,1.75 -2.82,2.33 -1.16,0.49 -2.52,0.78 -4.08,0.78 -2.62,0 -4.56,-0.58 -5.82,-1.65 -1.36,-1.07 -2.24,-2.62 -2.62,-4.66zm27.77 -5.54c2.23,-0.29 3.88,-0.68 5.04,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.97,-2.91 -0.77,-0.68 -2.03,-1.07 -3.78,-1.07 -0.1,0 -0.2,0 -0.29,0 -1.36,0 -2.43,0.29 -3.11,0.78 -0.78,0.58 -1.36,1.55 -1.65,2.91l-3.3 -0.39c0.29,-1.45 0.77,-2.52 1.45,-3.4 0.68,-0.87 1.65,-1.55 3.01,-1.94 1.07,-0.39 2.24,-0.68 3.6,-0.68 0.29,-0.09 0.58,-0.09 0.77,-0.09 1.75,0 3.11,0.29 4.08,0.67 1.07,0.39 1.85,0.88 2.33,1.46 0.49,0.58 0.87,1.36 1.07,2.23 0.1,0.59 0.19,1.65 0.19,3.11l0 4.37c0,3.11 0,5.15 0.2,5.92 0.09,0.78 0.39,1.66 0.87,2.33l-3.5 0c-0.38,-0.67 -0.58,-1.45 -0.67,-2.42 -1.27,1.07 -2.43,1.84 -3.6,2.23 -0.58,0.19 -1.16,0.39 -1.74,0.49 -0.59,0.09 -1.27,0.19 -1.95,0.19 -2.13,0 -3.78,-0.58 -4.95,-1.55 -1.16,-1.07 -1.75,-2.43 -1.75,-4.08 0,-0.97 0.3,-1.85 0.68,-2.62 0.49,-0.78 0.98,-1.46 1.75,-1.95 0.68,-0.48 1.46,-0.87 2.33,-1.06 0.68,-0.2 1.65,-0.39 3.01,-0.49 0.29,-0.1 0.58,-0.1 0.88,-0.1zm0 9.13c0.87,-0.1 1.65,-0.39 2.33,-0.78 0.97,-0.48 1.74,-1.26 2.23,-2.23 0.29,-0.77 0.48,-1.94 0.48,-3.4l0 -1.16c-1.16,0.48 -2.81,0.87 -5.04,1.16 -0.1,0 -0.3,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.92,0.68 -0.58,0.19 -0.97,0.58 -1.26,1.07 -0.39,0.48 -0.48,0.97 -0.48,1.55 0,0.87 0.29,1.65 0.97,2.23 0.68,0.59 1.65,0.88 3.01,0.88 0.39,0 0.68,0 1.07,-0.1zm-634.43 2.23l0 0 0 -27.09 5.44 0 6.4 19.23c0.59,1.75 1.07,3.11 1.27,3.98 0.39,-0.97 0.87,-2.43 1.45,-4.37l6.51 -18.84 4.85 0 0 27.09 -3.49 0 0 -22.62 -7.87 22.62 -3.2 0 -7.86 -23.01 0 23.01 -3.5 0zm52.24 0l0 0 0 -27.09 3.39 0 0 27.09 -3.39 0zm-13.01 -11.65l5.53 0c-0.19,-1.65 -0.58,-2.91 -1.26,-3.78 -1.07,-1.27 -2.43,-1.85 -4.08,-1.85 -0.1,0 -0.1,0 -0.19,0 -1.46,0 -2.72,0.49 -3.69,1.46 -1.07,1.06 -1.65,2.42 -1.75,4.17l5.44 0zm0 9.42c0.09,0 0.19,0 0.29,0 1.26,0 2.23,-0.39 3.1,-0.97 0.88,-0.68 1.56,-1.65 2.14,-3.11l3.4 0.39c-0.58,2.04 -1.55,3.59 -3.01,4.76 -1.46,1.07 -3.4,1.65 -5.63,1.65 -0.1,0 -0.2,0 -0.29,0 -2.72,-0.1 -4.96,-0.97 -6.51,-2.72 -1.75,-1.75 -2.52,-4.17 -2.52,-7.38 0,-3.3 0.77,-5.92 2.52,-7.67 1.65,-1.84 3.89,-2.72 6.51,-2.81 0.09,0 0.09,0 0.09,0 2.63,0 4.76,0.97 6.41,2.71 1.75,1.75 2.53,4.28 2.53,7.58 0,0.19 0,0.48 0,0.87l-9.03 0 -5.64 0c0.1,2.14 0.78,3.79 1.85,4.95 1.07,1.07 2.33,1.65 3.79,1.75zm42.43 9.81l0 0 -0.39 -3.11c0.77,0.19 1.36,0.29 1.94,0.29 0.68,0 1.26,-0.1 1.75,-0.39 0.48,-0.19 0.78,-0.58 1.07,-1.06 0.29,-0.3 0.58,-1.07 1.07,-2.43 0,-0.2 0.09,-0.49 0.29,-0.78l-7.48 -19.71 3.59 0 4.08 11.36c0.58,1.46 1.07,3.01 1.46,4.56 0.38,-1.55 0.77,-3.01 1.36,-4.46l4.17 -11.46 3.3 0 -7.47 20c-0.78,2.14 -1.36,3.6 -1.85,4.47 -0.58,1.07 -1.26,1.84 -2.04,2.33 -0.77,0.58 -1.65,0.78 -2.72,0.78 -0.68,0 -1.36,-0.1 -2.13,-0.39zm-13.4 -18.94c2.23,-0.29 3.88,-0.68 5.05,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.97,-2.91 -0.88,-0.68 -2.14,-1.07 -3.79,-1.07 -0.1,0 -0.19,0 -0.29,0 -1.46,0 -2.43,0.29 -3.21,0.78 -0.68,0.58 -1.26,1.55 -1.65,2.91l-3.2 -0.39c0.29,-1.45 0.78,-2.52 1.46,-3.4 0.68,-0.87 1.65,-1.55 2.91,-1.94 1.07,-0.39 2.33,-0.68 3.69,-0.68 0.19,-0.09 0.48,-0.09 0.78,-0.09 1.64,0 3,0.29 4.07,0.67 1.07,0.39 1.85,0.88 2.33,1.46 0.49,0.58 0.88,1.36 1.07,2.23 0.1,0.59 0.1,1.65 0.1,3.11l0 4.37c0,3.11 0.1,5.15 0.29,5.92 0.1,0.78 0.39,1.66 0.78,2.33l-3.5 0c-0.29,-0.67 -0.48,-1.45 -0.58,-2.42 -1.26,1.07 -2.43,1.84 -3.59,2.23 -0.59,0.19 -1.17,0.39 -1.75,0.49 -0.68,0.09 -1.26,0.19 -1.94,0.19 -2.14,0 -3.79,-0.58 -4.96,-1.55 -1.16,-1.07 -1.74,-2.43 -1.74,-4.08 0,-0.97 0.19,-1.85 0.68,-2.62 0.38,-0.78 0.97,-1.46 1.65,-1.95 0.77,-0.48 1.55,-0.87 2.42,-1.06 0.68,-0.2 1.65,-0.39 3.01,-0.49 0.3,-0.1 0.59,-0.1 0.88,-0.1zm0 9.13c0.78,-0.1 1.55,-0.39 2.33,-0.78 0.97,-0.48 1.65,-1.26 2.13,-2.23 0.39,-0.77 0.59,-1.94 0.59,-3.4l0 -1.16c-1.17,0.48 -2.82,0.87 -5.05,1.16 -0.2,0 -0.29,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.91,0.68 -0.59,0.19 -1.07,0.58 -1.36,1.07 -0.29,0.48 -0.39,0.97 -0.39,1.55 0,0.87 0.29,1.65 0.97,2.23 0.68,0.59 1.65,0.88 2.91,0.88 0.39,0 0.78,0 1.17,-0.1zm93.89 2.23l0 0 0 -27.09 3.69 0 14.27 21.27 0 -21.27 3.4 0 0 27.09 -3.69 0 -14.17 -21.26 0 21.26 -3.5 0zm-19.32 0l0 0 0 -19.61 3.4 0 0 19.61 -3.4 0zm0 -23.2l0 0 0 -3.89 3.4 0 0 3.89 -3.4 0zm-21.07 23.2l0 0 0 -19.61 3.01 0 0 2.82c1.45,-2.14 3.59,-3.3 6.31,-3.3 1.17,0 2.23,0.29 3.2,0.67 0.97,0.39 1.75,0.98 2.24,1.66 0.48,0.67 0.87,1.55 1.06,2.52 0.1,0.58 0.2,1.65 0.2,3.2l0 12.04 -3.4 0 0 -11.94c0,-1.36 -0.1,-2.33 -0.39,-3.01 -0.19,-0.68 -0.68,-1.26 -1.36,-1.65 -0.68,-0.39 -1.36,-0.58 -2.33,-0.58 -1.36,0 -2.62,0.48 -3.59,1.36 -1.07,0.87 -1.55,2.62 -1.55,5.14l0 10.68 -3.4 0zm-13.5 -11.36c2.23,-0.29 3.89,-0.68 5.05,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.97,-2.91 -0.87,-0.68 -2.14,-1.07 -3.79,-1.07 -0.09,0 -0.19,0 -0.29,0 -1.45,0 -2.52,0.29 -3.2,0.78 -0.68,0.58 -1.26,1.55 -1.65,2.91l-3.21 -0.39c0.29,-1.45 0.78,-2.52 1.46,-3.4 0.68,-0.87 1.65,-1.55 2.91,-1.94 1.07,-0.39 2.33,-0.68 3.69,-0.68 0.2,-0.09 0.49,-0.09 0.78,-0.09 1.65,0 3.01,0.29 4.08,0.67 1.07,0.39 1.84,0.88 2.33,1.46 0.48,0.58 0.77,1.36 1.06,2.23 0.1,0.59 0.1,1.65 0.1,3.11l0 4.37c0,3.11 0.1,5.15 0.2,5.92 0.19,0.78 0.48,1.66 0.87,2.33l-3.49 0c-0.3,-0.67 -0.59,-1.45 -0.68,-2.42 -1.17,1.07 -2.34,1.84 -3.5,2.23 -0.58,0.19 -1.16,0.39 -1.75,0.49 -0.68,0.09 -1.26,0.19 -1.94,0.19 -2.14,0 -3.79,-0.58 -4.95,-1.55 -1.17,-1.07 -1.75,-2.43 -1.75,-4.08 0,-0.97 0.19,-1.85 0.68,-2.62 0.39,-0.78 0.97,-1.46 1.65,-1.95 0.78,-0.48 1.55,-0.87 2.43,-1.06 0.68,-0.2 1.65,-0.39 2.91,-0.49 0.39,-0.1 0.68,-0.1 0.97,-0.1zm0 9.13c0.78,-0.1 1.56,-0.39 2.23,-0.78 1.07,-0.48 1.75,-1.26 2.24,-2.23 0.39,-0.77 0.58,-1.94 0.58,-3.4l0 -1.16c-1.16,0.48 -2.91,0.87 -5.05,1.16 -0.19,0 -0.29,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.91,0.68 -0.58,0.19 -1.07,0.58 -1.36,1.07 -0.29,0.48 -0.48,0.97 -0.48,1.55 0,0.87 0.38,1.65 1.06,2.23 0.68,0.59 1.65,0.88 2.92,0.88 0.39,0 0.77,0 1.16,-0.1zm88.84 -9.42l5.54 0c-0.19,-1.65 -0.58,-2.91 -1.26,-3.78 -1.07,-1.27 -2.43,-1.85 -4.18,-1.85 0,0 0,0 -0.1,0 -1.45,0 -2.71,0.49 -3.69,1.46 -1.06,1.06 -1.65,2.42 -1.74,4.17l5.43 0zm0 9.42c0.1,0 0.2,0 0.3,0 1.26,0 2.23,-0.39 3.1,-0.97 0.88,-0.68 1.56,-1.65 2.04,-3.11l3.5 0.39c-0.59,2.04 -1.56,3.59 -3.01,4.76 -1.46,1.07 -3.4,1.65 -5.63,1.65 -0.1,0 -0.2,0 -0.3,0 -2.71,-0.1 -4.95,-0.97 -6.6,-2.72 -1.65,-1.75 -2.52,-4.17 -2.52,-7.38 0,-3.3 0.87,-5.92 2.62,-7.67 1.65,-1.84 3.88,-2.72 6.5,-2.81 0,0 0.1,0 0.1,0 2.62,0 4.76,0.97 6.41,2.71 1.65,1.75 2.52,4.28 2.52,7.58 0,0.19 0,0.48 0,0.87l-9.03 0 -5.63 0c0.1,2.14 0.68,3.79 1.85,4.95 0.97,1.07 2.33,1.65 3.78,1.75zm20.59 -0.48c1.55,0 2.81,-0.59 3.88,-1.75 1.07,-1.17 1.56,-3.01 1.56,-5.54 0,-2.42 -0.59,-4.27 -1.65,-5.43 -0.98,-1.27 -2.24,-1.85 -3.79,-1.85 -1.56,0 -2.72,0.58 -3.79,1.75 -1.07,1.26 -1.55,3.01 -1.55,5.44 0,2.52 0.48,4.37 1.55,5.63 0.97,1.16 2.23,1.75 3.79,1.75zm0 7.96c1.36,-0.1 2.43,-0.39 3.2,-0.97 0.88,-0.59 1.36,-1.46 1.75,-2.53 0.1,-0.68 0.2,-2.13 0.2,-4.27 -1.36,1.65 -3.11,2.43 -5.15,2.52 -0.1,0 -0.19,0 -0.29,0 -2.72,0 -4.76,-0.97 -6.31,-2.91 -1.46,-1.94 -2.14,-4.27 -2.14,-6.99 0,-1.84 0.29,-3.59 0.97,-5.15 0.68,-1.65 1.65,-2.81 2.91,-3.69 1.36,-0.87 2.82,-1.35 4.57,-1.35 0.1,0 0.19,0 0.29,0 2.23,0.09 3.98,1.06 5.44,2.81l0 -2.33 3.1 0 0 16.99c0,3.01 -0.29,5.25 -0.97,6.51 -0.58,1.26 -1.55,2.23 -2.91,3.01 -1.26,0.68 -2.82,1.07 -4.66,1.07 -0.1,0 -0.19,0 -0.39,0 -2.33,0 -4.17,-0.49 -5.63,-1.56 -1.46,-1.07 -2.14,-2.62 -2.14,-4.75l3.3 0.48c0.1,0.97 0.49,1.75 1.07,2.14 0.88,0.68 2.04,0.97 3.4,0.97 0.2,0 0.29,0 0.39,0zm77 -5.25l0 0 0 -27.09 3.59 0 0 13.5 13.4 -13.5 4.85 0 -11.36 10.98 11.85 16.11 -4.66 0 -9.71 -13.69 -4.37 4.37 0 9.32 -3.59 0zm-20.49 0l0 0 0 -3.78 3.79 0 0 3.78c0,1.46 -0.2,2.53 -0.78,3.4 -0.49,0.88 -1.26,1.56 -2.33,1.94l-0.87 -1.35c0.68,-0.3 1.16,-0.78 1.55,-1.36 0.29,-0.59 0.48,-1.46 0.48,-2.63l-1.84 0zm-9.23 0l0 0 0 -19.61 3.31 0 0 19.61 -3.31 0zm0 -23.2l0 0 0 -3.89 3.31 0 0 3.89 -3.31 0zm-12.71 23.2l0 0 0 -19.61 3 0 0 3.01c0.78,-1.46 1.46,-2.33 2.14,-2.82 0.59,-0.38 1.36,-0.67 2.14,-0.67 1.07,0 2.23,0.38 3.4,1.06l-1.17 3.11c-0.78,-0.48 -1.65,-0.68 -2.43,-0.68 -0.68,0 -1.35,0.2 -1.94,0.68 -0.58,0.39 -0.97,0.97 -1.26,1.75 -0.39,1.26 -0.58,2.52 -0.58,3.88l0 10.29 -3.3 0zm-13.02 -11.65l5.44 0c-0.1,-1.65 -0.48,-2.91 -1.16,-3.78 -1.07,-1.27 -2.53,-1.85 -4.18,-1.85 0,0 -0.1,0 -0.1,0 -1.45,0 -2.72,0.49 -3.78,1.46 -0.97,1.06 -1.56,2.42 -1.65,4.17l5.43 0zm0 9.42c0.1,0 0.2,0 0.3,0 1.16,0 2.23,-0.39 3.1,-0.97 0.88,-0.68 1.56,-1.65 2.04,-3.11l3.4 0.39c-0.48,2.04 -1.46,3.59 -3.01,4.76 -1.46,1.07 -3.3,1.65 -5.53,1.65 -0.1,0 -0.2,0 -0.3,0 -2.72,-0.1 -4.95,-0.97 -6.6,-2.72 -1.65,-1.75 -2.52,-4.17 -2.52,-7.38 0,-3.3 0.87,-5.92 2.62,-7.67 1.65,-1.84 3.78,-2.72 6.5,-2.81 0,0 0,0 0.1,0 2.62,0 4.76,0.97 6.41,2.71 1.65,1.75 2.52,4.28 2.52,7.58 0,0.19 0,0.48 -0.09,0.87l-8.94 0 -5.63 0c0.1,2.14 0.68,3.79 1.85,4.95 0.97,1.07 2.23,1.65 3.78,1.75zm88.26 -9.42l5.54 0c-0.19,-1.65 -0.58,-2.91 -1.26,-3.78 -1.07,-1.27 -2.43,-1.85 -4.18,-1.85 0,0 0,0 -0.1,0 -1.45,0 -2.71,0.49 -3.68,1.46 -1.07,1.06 -1.66,2.42 -1.75,4.17l5.43 0zm0 9.42c0.1,0 0.2,0 0.3,0 1.16,0 2.23,-0.39 3.1,-0.97 0.88,-0.68 1.56,-1.65 2.04,-3.11l3.5 0.39c-0.59,2.04 -1.56,3.59 -3.01,4.76 -1.56,1.07 -3.4,1.65 -5.63,1.65 -0.1,0 -0.2,0 -0.3,0 -2.71,-0.1 -4.95,-0.97 -6.5,-2.72 -1.65,-1.75 -2.52,-4.17 -2.52,-7.38 0,-3.3 0.87,-5.92 2.62,-7.67 1.55,-1.84 3.78,-2.72 6.4,-2.81 0,0 0.1,0 0.1,0 2.62,0 4.76,0.97 6.41,2.71 1.65,1.75 2.53,4.28 2.53,7.58 0,0.19 0,0.48 0,0.87l-9.04 0 -5.53 0c0,2.14 0.58,3.79 1.75,4.95 0.97,1.07 2.23,1.65 3.78,1.75zm21.37 -17.77c0.19,-0.09 0.29,-0.09 0.38,-0.09 1.27,0 2.34,0.29 3.5,0.77 1.07,0.49 1.94,1.17 2.62,2.04 0.68,0.88 1.26,1.94 1.65,3.2 0.39,1.27 0.58,2.63 0.58,3.99 0,3.39 -0.87,6.02 -2.52,7.76 -1.65,1.85 -3.69,2.82 -6.02,2.82 0,0 -0.1,0 -0.19,0 -2.14,-0.1 -3.89,-1.07 -5.25,-2.91l0 2.42 -3.01 0 0 -27.09 3.3 0 0 9.71c1.27,-1.65 3.01,-2.62 4.96,-2.62zm0 17.77c1.45,-0.1 2.72,-0.68 3.69,-1.94 1.06,-1.26 1.65,-3.11 1.65,-5.63 0,-2.53 -0.49,-4.47 -1.56,-5.63 -0.97,-1.27 -2.23,-1.85 -3.69,-1.85 0,0 0,0 -0.09,0 -1.36,0 -2.63,0.58 -3.69,1.85 -1.07,1.26 -1.56,3.1 -1.56,5.53 0,2.33 0.29,3.98 0.97,5.05 1.07,1.75 2.43,2.62 4.28,2.62zm33.88 2.23l0 0 0 -19.61 2.91 0 0 2.82c1.46,-2.14 3.6,-3.3 6.32,-3.3 1.16,0 2.23,0.29 3.2,0.67 1.07,0.39 1.75,0.98 2.24,1.66 0.48,0.67 0.87,1.55 1.06,2.52 0.1,0.58 0.2,1.65 0.2,3.2l0 12.04 -3.3 0 0 -11.94c0,-1.36 -0.2,-2.33 -0.39,-3.01 -0.29,-0.68 -0.78,-1.26 -1.46,-1.65 -0.58,-0.39 -1.36,-0.58 -2.33,-0.58 -1.36,0 -2.62,0.48 -3.59,1.36 -1.07,0.87 -1.56,2.62 -1.56,5.14l0 10.68 -3.3 0zm-13.59 -11.36c2.23,-0.29 3.88,-0.68 5.05,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.97,-2.91 -0.88,-0.68 -2.14,-1.07 -3.79,-1.07 -0.1,0 -0.19,0 -0.29,0 -1.46,0 -2.43,0.29 -3.21,0.78 -0.68,0.58 -1.26,1.55 -1.65,2.91l-3.2 -0.39c0.29,-1.45 0.78,-2.52 1.46,-3.4 0.68,-0.87 1.65,-1.55 2.91,-1.94 1.07,-0.39 2.33,-0.68 3.69,-0.68 0.19,-0.09 0.48,-0.09 0.78,-0.09 1.65,0 3.01,0.29 4.07,0.67 1.07,0.39 1.85,0.88 2.33,1.46 0.49,0.58 0.88,1.36 1.07,2.23 0.1,0.59 0.1,1.65 0.1,3.11l0 4.37c0,3.11 0.1,5.15 0.29,5.92 0.1,0.78 0.39,1.66 0.78,2.33l-3.5 0c-0.29,-0.67 -0.48,-1.45 -0.58,-2.42 -1.26,1.07 -2.43,1.84 -3.59,2.23 -0.58,0.19 -1.17,0.39 -1.75,0.49 -0.68,0.09 -1.26,0.19 -1.94,0.19 -2.14,0 -3.79,-0.58 -4.95,-1.55 -1.17,-1.07 -1.75,-2.43 -1.75,-4.08 0,-0.97 0.19,-1.85 0.68,-2.62 0.39,-0.78 0.97,-1.46 1.65,-1.95 0.78,-0.48 1.55,-0.87 2.42,-1.06 0.68,-0.2 1.66,-0.39 3.02,-0.49 0.29,-0.1 0.58,-0.1 0.87,-0.1zm0 9.13c0.78,-0.1 1.55,-0.39 2.23,-0.78 1.07,-0.48 1.75,-1.26 2.24,-2.23 0.38,-0.77 0.58,-1.94 0.58,-3.4l0 -1.16c-1.17,0.48 -2.91,0.87 -5.05,1.16 -0.2,0 -0.29,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.91,0.68 -0.59,0.19 -1.07,0.58 -1.36,1.07 -0.29,0.48 -0.39,0.97 -0.39,1.55 0,0.87 0.29,1.65 0.97,2.23 0.68,0.59 1.65,0.88 2.91,0.88 0.39,0 0.78,0 1.17,-0.1zm42.14 -0.48c1.55,0 2.81,-0.59 3.79,-1.75 1.06,-1.17 1.55,-3.01 1.55,-5.54 0,-2.42 -0.49,-4.27 -1.55,-5.43 -1.07,-1.27 -2.34,-1.85 -3.79,-1.85 0,0 0,0 -0.1,0 -1.45,0 -2.72,0.58 -3.78,1.75 -0.98,1.26 -1.56,3.01 -1.56,5.44 0,2.52 0.58,4.37 1.56,5.63 1.06,1.16 2.33,1.75 3.88,1.75zm0 7.96c1.36,-0.1 2.43,-0.39 3.2,-0.97 0.78,-0.59 1.36,-1.46 1.65,-2.53 0.2,-0.68 0.29,-2.13 0.29,-4.27 -1.45,1.65 -3.1,2.43 -5.14,2.52 -0.1,0 -0.19,0 -0.29,0 -2.72,0 -4.86,-0.97 -6.31,-2.91 -1.46,-1.94 -2.24,-4.27 -2.24,-6.99 0,-1.84 0.39,-3.59 1.07,-5.15 0.68,-1.65 1.65,-2.81 2.91,-3.69 1.27,-0.87 2.82,-1.35 4.57,-1.35 0.1,0 0.19,0 0.29,0 2.14,0.09 3.98,1.06 5.44,2.81l0 -2.33 3.1 0 0 16.99c0,3.01 -0.39,5.25 -0.97,6.51 -0.68,1.26 -1.65,2.23 -3.01,3.01 -1.26,0.68 -2.72,1.07 -4.56,1.07 -0.1,0 -0.29,0 -0.39,0 -2.33,0 -4.27,-0.49 -5.63,-1.56 -1.46,-1.07 -2.14,-2.62 -2.14,-4.75l3.21 0.48c0.19,0.97 0.58,1.75 1.16,2.14 0.88,0.68 1.94,0.97 3.4,0.97 0.1,0 0.29,0 0.39,0zm21.07 -7.96c1.55,0 2.81,-0.59 3.78,-1.75 1.07,-1.17 1.56,-3.01 1.56,-5.54 0,-2.42 -0.49,-4.27 -1.56,-5.43 -1.06,-1.27 -2.32,-1.85 -3.78,-1.85 0,0 -0.1,0 -0.1,0 -1.46,0 -2.72,0.58 -3.78,1.75 -0.98,1.26 -1.56,3.01 -1.56,5.44 0,2.52 0.58,4.37 1.56,5.63 1.06,1.16 2.32,1.75 3.88,1.75zm0 7.96c1.36,-0.1 2.43,-0.39 3.2,-0.97 0.78,-0.59 1.36,-1.46 1.65,-2.53 0.2,-0.68 0.29,-2.13 0.29,-4.27 -1.45,1.65 -3.1,2.43 -5.14,2.52 -0.1,0 -0.2,0 -0.29,0 -2.72,0 -4.86,-0.97 -6.31,-2.91 -1.46,-1.94 -2.24,-4.27 -2.24,-6.99 0,-1.84 0.39,-3.59 1.07,-5.15 0.68,-1.65 1.65,-2.81 2.91,-3.69 1.27,-0.87 2.82,-1.35 4.57,-1.35 0.09,0 0.19,0 0.29,0 2.14,0.09 3.98,1.06 5.44,2.81l0 -2.33 3 0 0 16.99c0,3.01 -0.28,5.25 -0.87,6.51 -0.68,1.26 -1.65,2.23 -3.01,3.01 -1.26,0.68 -2.81,1.07 -4.56,1.07 -0.1,0 -0.29,0 -0.39,0 -2.33,0 -4.27,-0.49 -5.63,-1.56 -1.46,-1.07 -2.23,-2.62 -2.14,-4.75l3.21 0.48c0.19,0.97 0.58,1.75 1.16,2.14 0.78,0.68 1.94,0.97 3.4,0.97 0.1,0 0.29,0 0.39,0zm20.97 -16.61c2.24,-0.29 3.98,-0.68 5.05,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.87,-2.91 -0.88,-0.68 -2.14,-1.07 -3.79,-1.07 -0.1,0 -0.29,0 -0.39,0 -1.36,0 -2.43,0.29 -3.11,0.78 -0.77,0.58 -1.26,1.55 -1.65,2.91l-3.3 -0.39c0.39,-1.45 0.88,-2.52 1.46,-3.4 0.68,-0.87 1.75,-1.55 3.01,-1.94 1.07,-0.39 2.23,-0.68 3.59,-0.68 0.29,-0.09 0.58,-0.09 0.87,-0.09 1.66,0 3.02,0.29 4.08,0.67 1.07,0.39 1.75,0.88 2.33,1.46 0.49,0.58 0.78,1.36 0.97,2.23 0.1,0.59 0.2,1.65 0.2,3.11l0 4.37c0,3.11 0.1,5.15 0.19,5.92 0.2,0.78 0.39,1.66 0.88,2.33l-3.5 0c-0.39,-0.67 -0.58,-1.45 -0.68,-2.42 -1.26,1.07 -2.43,1.84 -3.59,2.23 -0.49,0.19 -1.17,0.39 -1.75,0.49 -0.58,0.09 -1.26,0.19 -1.84,0.19 -2.24,0 -3.89,-0.58 -5.05,-1.55 -1.07,-1.07 -1.65,-2.43 -1.65,-4.08 0,-0.97 0.19,-1.85 0.58,-2.62 0.49,-0.78 1.07,-1.46 1.75,-1.95 0.68,-0.48 1.55,-0.87 2.42,-1.06 0.59,-0.2 1.65,-0.39 2.92,-0.49 0.29,-0.1 0.58,-0.1 0.87,-0.1zm0 9.13c0.87,-0.1 1.65,-0.39 2.33,-0.78 0.97,-0.48 1.75,-1.26 2.23,-2.23 0.3,-0.77 0.49,-1.94 0.49,-3.4l0 -1.16c-1.16,0.48 -2.81,0.87 -5.05,1.16 -0.1,0 -0.19,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.91,0.68 -0.49,0.19 -0.97,0.58 -1.26,1.07 -0.29,0.48 -0.49,0.97 -0.49,1.55 0,0.87 0.39,1.65 1.07,2.23 0.58,0.59 1.65,0.88 2.91,0.88 0.39,0 0.78,0 1.07,-0.1zm34.66 2.23l0 0 0 -19.61 3.02 0 0 2.82c1.45,-2.14 3.49,-3.3 6.21,-3.3 1.16,0 2.33,0.29 3.3,0.67 0.97,0.39 1.75,0.98 2.23,1.66 0.49,0.67 0.78,1.55 0.97,2.52 0.2,0.58 0.2,1.65 0.2,3.2l0 12.04 -3.3 0 0 -11.94c0,-1.36 -0.1,-2.33 -0.39,-3.01 -0.29,-0.68 -0.68,-1.26 -1.36,-1.65 -0.68,-0.39 -1.46,-0.58 -2.33,-0.58 -1.46,0 -2.62,0.48 -3.69,1.36 -1.07,0.87 -1.55,2.62 -1.55,5.14l0 10.68 -3.31 0zm-13.59 -11.36c2.23,-0.29 3.98,-0.68 5.05,-1.06 0,-0.49 0,-0.78 0,-0.88 0,-1.36 -0.29,-2.33 -0.87,-2.91 -0.88,-0.68 -2.14,-1.07 -3.79,-1.07 -0.2,0 -0.29,0 -0.39,0 -1.36,0 -2.43,0.29 -3.11,0.78 -0.77,0.58 -1.26,1.55 -1.65,2.91l-3.3 -0.39c0.29,-1.45 0.78,-2.52 1.46,-3.4 0.68,-0.87 1.75,-1.55 3.01,-1.94 1.07,-0.39 2.23,-0.68 3.59,-0.68 0.29,-0.09 0.58,-0.09 0.88,-0.09 1.65,0 3,0.29 4.07,0.67 0.97,0.39 1.75,0.88 2.24,1.46 0.58,0.58 0.87,1.36 1.06,2.23 0.1,0.59 0.2,1.65 0.2,3.11l0 4.37c0,3.11 0.1,5.15 0.19,5.92 0.1,0.78 0.39,1.66 0.88,2.33l-3.5 0c-0.39,-0.67 -0.58,-1.45 -0.68,-2.42 -1.26,1.07 -2.43,1.84 -3.59,2.23 -0.58,0.19 -1.17,0.39 -1.75,0.49 -0.58,0.09 -1.26,0.19 -1.84,0.19 -2.24,0 -3.89,-0.58 -5.05,-1.55 -1.17,-1.07 -1.75,-2.43 -1.75,-4.08 0,-0.97 0.29,-1.85 0.68,-2.62 0.48,-0.78 1.07,-1.46 1.75,-1.95 0.68,-0.48 1.45,-0.87 2.43,-1.06 0.58,-0.2 1.55,-0.39 2.91,-0.49 0.29,-0.1 0.58,-0.1 0.87,-0.1zm0 9.13c0.88,-0.1 1.65,-0.39 2.33,-0.78 0.97,-0.48 1.75,-1.26 2.24,-2.23 0.29,-0.77 0.48,-1.94 0.48,-3.4l0 -1.16c-1.17,0.48 -2.82,0.87 -5.05,1.16 -0.09,0 -0.19,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.91,0.68 -0.58,0.19 -0.97,0.58 -1.26,1.07 -0.29,0.48 -0.49,0.97 -0.49,1.55 0,0.87 0.39,1.65 0.97,2.23 0.68,0.59 1.65,0.88 3.01,0.88 0.39,0 0.68,0 1.07,-0.1zm55.06 -24.86l0.58 0c2.04,0 3.69,0.3 4.95,0.88 1.26,0.48 2.23,1.36 2.91,2.52 0.78,1.07 1.07,2.33 1.07,3.6 0,1.06 -0.29,2.23 -0.87,3.2 -0.68,1.07 -1.66,1.84 -2.92,2.52 1.65,0.49 2.92,1.27 3.79,2.43 0.88,1.17 1.36,2.52 1.36,4.08 0,1.26 -0.29,2.52 -0.78,3.59 -0.58,1.07 -1.26,1.94 -2.04,2.53 -0.77,0.58 -1.74,0.97 -2.91,1.36 -1.26,0.29 -2.72,0.38 -4.47,0.38l-0.67 0 -9.62 0 0 -27.09 9.62 0zm0 11.36c1.45,0 2.62,-0.09 3.3,-0.29 0.87,-0.29 1.55,-0.68 2.04,-1.36 0.48,-0.58 0.67,-1.36 0.67,-2.33 0,-0.87 -0.19,-1.74 -0.67,-2.42 -0.39,-0.68 -1.07,-1.07 -1.85,-1.36 -0.68,-0.2 -1.85,-0.39 -3.49,-0.39 -0.2,0 -0.39,0 -0.59,0l-5.44 0 0 8.15 5.83 0c0.1,0 0.1,0 0.2,0zm0 12.53l0.67 0c1.17,0 2.04,0 2.53,-0.1 0.78,-0.19 1.46,-0.39 2.04,-0.77 0.58,-0.3 0.97,-0.78 1.36,-1.46 0.38,-0.68 0.58,-1.46 0.58,-2.33 0,-0.97 -0.29,-1.85 -0.78,-2.62 -0.58,-0.78 -1.26,-1.26 -2.23,-1.56 -0.87,-0.29 -2.23,-0.48 -3.88,-0.48l-0.29 0 -6.03 0 0 9.32 6.03 0zm36.41 3.2l0 0 0 -19.61 3.01 0 0 2.82c1.45,-2.14 3.49,-3.3 6.21,-3.3 1.26,0 2.33,0.29 3.3,0.67 0.97,0.39 1.75,0.98 2.23,1.66 0.49,0.67 0.88,1.55 1.07,2.52 0.1,0.58 0.2,1.65 0.2,3.2l0 12.04 -3.4 0 0 -11.94c0,-1.36 -0.1,-2.33 -0.39,-3.01 -0.19,-0.68 -0.68,-1.26 -1.36,-1.65 -0.68,-0.39 -1.46,-0.58 -2.33,-0.58 -1.36,0 -2.62,0.48 -3.69,1.36 -0.97,0.87 -1.55,2.62 -1.55,5.14l0 10.68 -3.3 0zm-13.6 -11.36c2.24,-0.29 3.98,-0.68 5.05,-1.06 0.1,-0.49 0.1,-0.78 0.1,-0.88 0,-1.36 -0.39,-2.33 -0.97,-2.91 -0.88,-0.68 -2.14,-1.07 -3.79,-1.07 -0.1,0 -0.29,0 -0.39,0 -1.36,0 -2.42,0.29 -3.1,0.78 -0.78,0.58 -1.27,1.55 -1.66,2.91l-3.2 -0.39c0.29,-1.45 0.78,-2.52 1.46,-3.4 0.68,-0.87 1.65,-1.55 2.91,-1.94 1.07,-0.39 2.23,-0.68 3.59,-0.68 0.29,-0.09 0.58,-0.09 0.88,-0.09 1.65,0 3.01,0.29 4.07,0.67 1.07,0.39 1.85,0.88 2.33,1.46 0.49,0.58 0.78,1.36 0.98,2.23 0.09,0.59 0.19,1.65 0.19,3.11l0 4.37c0,3.11 0.1,5.15 0.19,5.92 0.2,0.78 0.49,1.66 0.88,2.33l-3.5 0c-0.29,-0.67 -0.58,-1.45 -0.68,-2.42 -1.16,1.07 -2.42,1.84 -3.49,2.23 -0.59,0.19 -1.17,0.39 -1.85,0.49 -0.58,0.09 -1.26,0.19 -1.84,0.19 -2.14,0 -3.79,-0.58 -4.95,-1.55 -1.17,-1.07 -1.75,-2.43 -1.75,-4.08 0,-0.97 0.19,-1.85 0.58,-2.62 0.49,-0.78 1.07,-1.46 1.75,-1.95 0.68,-0.48 1.55,-0.87 2.43,-1.06 0.58,-0.2 1.65,-0.39 2.91,-0.49 0.29,-0.1 0.58,-0.1 0.87,-0.1zm0 9.13c0.88,-0.1 1.65,-0.39 2.33,-0.78 0.97,-0.48 1.75,-1.26 2.24,-2.23 0.38,-0.77 0.48,-1.94 0.48,-3.4l0 -1.16c-1.07,0.48 -2.81,0.87 -5.05,1.16 -0.09,0 -0.19,0.1 -0.39,0.1 -1.36,0.19 -2.33,0.39 -2.81,0.68 -0.58,0.19 -1.07,0.58 -1.36,1.07 -0.29,0.48 -0.49,0.97 -0.49,1.55 0,0.87 0.39,1.65 1.07,2.23 0.58,0.59 1.65,0.88 2.92,0.88 0.38,0 0.77,0 1.06,-0.1zm42.24 -0.48c1.46,0 2.81,-0.59 3.79,-1.75 1.06,-1.17 1.55,-3.01 1.55,-5.54 0,-2.42 -0.49,-4.27 -1.55,-5.43 -1.07,-1.27 -2.33,-1.85 -3.79,-1.85 -0.1,0 -0.1,0 -0.1,0 -1.45,0 -2.72,0.58 -3.78,1.75 -1.07,1.26 -1.56,3.01 -1.56,5.44 0,2.52 0.49,4.37 1.56,5.63 1.06,1.16 2.33,1.75 3.88,1.75zm0 7.96c1.36,-0.1 2.33,-0.39 3.11,-0.97 0.87,-0.59 1.45,-1.46 1.74,-2.53 0.2,-0.68 0.2,-2.13 0.2,-4.27 -1.36,1.65 -3.01,2.43 -5.05,2.52 -0.1,0 -0.29,0 -0.39,0 -2.72,0 -4.76,-0.97 -6.21,-2.91 -1.46,-1.94 -2.24,-4.27 -2.24,-6.99 0,-1.84 0.29,-3.59 0.97,-5.15 0.68,-1.65 1.75,-2.81 3.01,-3.69 1.27,-0.87 2.82,-1.35 4.57,-1.35 0.1,0 0.19,0 0.29,0 2.13,0.09 3.98,1.06 5.44,2.81l0 -2.33 3.01 0 0 16.99c0,3.01 -0.29,5.25 -0.88,6.51 -0.68,1.26 -1.65,2.23 -3.01,3.01 -1.26,0.68 -2.81,1.07 -4.56,1.07 -0.19,0 -0.29,0 -0.39,0 -2.33,0 -4.27,-0.49 -5.73,-1.56 -1.45,-1.07 -2.13,-2.62 -2.04,-4.75l3.21 0.48c0.19,0.97 0.48,1.75 1.16,2.14 0.78,0.68 1.94,0.97 3.4,0.97 0.1,0 0.2,0 0.39,0z" />
                        </g>
                      </svg>
                    </button>
                  </div>
                </div>
              </h2>
              <h6 class="text-white r-h6"><span id="bni">085323681185 | 0275749954</span></h6>
              <p class="mb-4 text-white"> a/n Astri Artiningsih</p>
              <div class="d-flex justify-content-start">
                <div>
                  <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="085323681185">Salin OVO</button>
                </div>
                <div class="px-2">
                  <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#bootstrapBasicModal" data-clipboard-text="0275749954">Salin BNI</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pb-3"></div>
    <!-- Kirim Hadiah -->
    <div class="container text-center direction-rtl">
      <div class="order-done-content">
        <svg class="bi bi-gift text-secondary mb-4" xmlns="http://www.w3.org/2000/svg" width="60" height="60" fill="currentColor" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M3 2.5a2.5 2.5 0 0 1 5 0 2.5 2.5 0 0 1 5 0v.006c0 .07 0 .27-.038.494H15a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a1.5 1.5 0 0 1-1.5 1.5h-11A1.5 1.5 0 0 1 1 14.5V7a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h2.038A2.968 2.968 0 0 1 3 2.506V2.5zm1.068.5H7v-.5a1.5 1.5 0 1 0-3 0c0 .085.002.274.045.43a.522.522 0 0 0 .023.07zM9 3h2.932a.56.56 0 0 0 .023-.07c.043-.156.045-.345.045-.43a1.5 1.5 0 0 0-3 0V3zM1 4v2h6V4H1zm8 0v2h6V4H9zm5 3H9v8h4.5a.5.5 0 0 0 .5-.5V7zm-7 8V7H2v7.5a.5.5 0 0 0 .5.5H7z" />
        </svg>
        <h5>Kirim Hadiah</h5>
        <p><strong class="bg-light text-dark shadow-sm me-2 fz-14">Jl. R. Dewi Sartika, No. 17 RT. 01 RW 06, Dusun Cibulan <small class="fw-lighter">(dekat SMPN 3 Banjar)</small>, Kec. Banjar, Kota Banjar 46311.</strong></p>
      </div>
    </div>
    <!-- Ucapan dan Doa -->
    <div class="container r-to-id" id="Ucapan">
      <!-- Contact Form -->
      <div class="card mb-3">
        <div class="card-body">
          <h5 class="mb-3">Ucapan & Do'a</h5>
          <div class="contact-form">
            <!-- form action template = http://localhost/api/comment/add -->
            <form action="https://api.irona.online/comment/add" method="POST">
              <div class="form-group mb-3">
                <input class="form-control" type="text" name="name" placeholder="Nama">
              </div>
              <div class="form-group mb-3">
                <input class="form-control" type="text" name="relation" placeholder="Relasi (Keluarga, Kerabat, lainnya)">
              </div>
              <div class="form-group mb-3">
                <select class="form-select" name="attendance">
                  <option value="sendiri">Hadir Sendiri</option>
                  <option value="berdua">Hadir Berdua</option>
                  <option value="banyak">Hadir Bersama Keluarga</option>
                  <option value="tidak">Maaf, Tidak Bisa Datang</option>
                </select>
              </div>
              <div class="form-group mb-3">
                <textarea class="form-control" name="message" cols="30" rows="10" placeholder="Tuliskan pesan atau ucapan doa untuk kami"></textarea>
              </div>
              <button class="btn btn-primary w-100">Kirim</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Chat User List -->
    <div class="container">
      <!-- Element Heading -->
      <div class="element-heading">
        <h6 class="ps-1">Ucapan Terbaru</h6>
      </div>

      <div class="data-scrollspy" data-bs-spy="scroll" data-bs-target="#scrollspyWrapper" data-bs-offset="0">
        <ul class="ps-0 chat-user-list">
          <?php foreach ($comments as $c) : ?>
            <!-- TEST Single Chat User -->
            <li class="p-3 <?php if ($c['attendance'] == 'tidak') {
                              echo 'offline';
                            } ?>">
              <a class="d-flex" href="#">
                <!-- Thumbnail -->
                <div class="chat-user-thumbnail me-3 shadow">
                  <span class="img-circle bg-info badge-avater badge-avater-lg"><?= $c['initials'] ?></span>
                  <span class="active-status"></span>
                </div>
                <!-- Info -->
                <div class="chat-user-info px-2">
                  <h6 class=" mb-0"><?= $c['name'] ?> <span class="d-inline-block badge bg-danger mb-2"><?= $c['relation'] ?></span></h6>
                  <div class="last-chat">
                    <!-- disalbe text-truncate class  -->
                    <p class=" mb-0"><?= $c['message'] ?></p>
                  </div>
                </div>
              </a>
              <!-- Options -->
              <div class="dropstart chat-options-btn">
                <button class="btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="true"><i class="bi bi-info-circle"></i></button>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="bi bi-person"></i><?= $c['attendance'] ?></a></li>
                </ul>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>

    </div>
    <div class="pb-3"></div>
  </div>
  <!-- Footer Nav -->
  <div class="footer-nav-area" id="footerNav">
    <div class="container px-0">
      <!-- =================================== -->
      <!-- Paste your Footer Content from here -->
      <!-- =================================== -->
      <!-- Footer Content -->
      <div class="footer-nav position-relative">
        <ul class="h-100 d-flex align-items-center justify-content-between ps-0">
          <li><a href="#Beranda">
              <svg class="bi bi-house" width="20" height="20" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"></path>
                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"></path>
              </svg><span>Beranda</span></a></li>
          <li><a href="#Mempelai">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
              </svg><span>Mempelai</span></a></li>
          <li><a href="#Acara">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-calendar-heart" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v11a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4V.5ZM1 14V4h14v10a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1Zm7-6.507c1.664-1.711 5.825 1.283 0 5.132-5.825-3.85-1.664-6.843 0-5.132Z" />
              </svg><span>Acara</span></a></li>
          <li><a href="#Galeri">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-camera" viewBox="0 0 16 16">
                <path d="M15 12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h1.172a3 3 0 0 0 2.12-.879l.83-.828A1 1 0 0 1 6.827 3h2.344a1 1 0 0 1 .707.293l.828.828A3 3 0 0 0 12.828 5H14a1 1 0 0 1 1 1v6zM2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2z" />
                <path d="M8 11a2.5 2.5 0 1 1 0-5 2.5 2.5 0 0 1 0 5zm0 1a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7zM3 6.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
              </svg><span>Galeri</span></a></li>
          <li><a href="#Ucapan">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-chat-square-text" viewBox="0 0 16 16">
                <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1h-2.5a2 2 0 0 0-1.6.8L8 14.333 6.1 11.8a2 2 0 0 0-1.6-.8H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                <path d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
              </svg><span>Ucapan</span></a></li>
        </ul>
      </div>
    </div>
  </div>


  <!-- Bootstrap Basic Modal -->
  <div class="modal fade" id="bootstrapBasicModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel">Berhasil di salin!</h6>
          <button class="btn btn-close p-1 ms-auto" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
      </div>
    </div>
  </div>
  <!-- Static Backdrop Modal -->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="staticBackdropLabel">Protokol Kesehatan</h6>
          <!-- <button class="btn btn-close p-1 ms-auto" type="button" data-bs-dismiss="modal" aria-label="Close"></button> -->
        </div>
        <div class="modal-body">
          <div class="row">
            <p class="mb-3 text-center"><em>Guna mencegah penyebaran Covid-19, diharapkan bagi tamu undangan untuk mematuhi protokol kesehatan di bawah ini : </em> </p>
          </div>
          <div class="row g-3">
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/cuci-tangan.svg" alt=""></div>
                <p class="mb-0">Cuci Tangan</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/masker.svg" alt=""></div>
                <p class="mb-0">Gunakan Masker</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/jarak.svg" alt=""></div>
                <p class="mb-0">Jaga Jarak</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/kerumunan.svg" alt=""></div>
                <p class="mb-0">Hindari Kerumunan</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/handsanitizer.svg" alt=""></div>
                <p class="mb-0">Gunakan Handsanitizer</p>
              </div>
            </div>
            <div class="col-4">
              <div class="feature-card mx-auto text-center">
                <div class="card mx-auto bg-gray"><img src="upload/icon/vaksin.svg" alt=""></div>
                <p class="mb-0">Sudah Vaksin</p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-bs-dismiss="modal">Mengerti</button>
        </div>
      </div>
    </div>
  </div>
  <!-- FULLSCREEN Backdrop Modal FIRST POP UP -->
  <div class="modal fade" id="openInvitationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="openInvitationModal" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
      <div class="modal-content">
        <div class="modal-body text-center bg-img bg-overlay" style="background-image: url('upload/img/landing.jpg');">
          <div class="h-100">
            <div class="row">
              <p class="lead d-block text-opacity-75 pt-5">Undangan Pernikahan</p>
            </div>
            <div class="row pt-5">
              <span class="display-3 r-font-brittany text-white">Astri <strong> & </strong> Irfan</span>
              <p class="d-block pt-3"><em>Minggu, 15 Mei 2022</em></p>

            </div>
            <div class="row pt-5">
              <div class="pt-5">
                <p class="d-block text-opacity-75">Kami mengundang <strong>Bapa/Ibu/Saudara/i</strong> untuk hadir di acara pernikahan kami</p>
                <br>
                <button onclick="togglePlay();" class="btn btn-outline-warning" type="button" data-bs-dismiss="modal">Buka Undangan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<!-- All JavaScript Files -->
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/slideToggle.min.js"></script>
<script src="js/internet-status.js"></script>
<script src="js/tiny-slider.js"></script>
<script src="js/baguetteBox.min.js"></script>
<script src="js/countdown.js"></script>
<script src="js/rangeslider.min.js"></script>
<script src="js/vanilla-dataTables.min.js"></script>
<script src="js/index.js"></script>
<script src="js/magic-grid.min.js"></script>
<script src="js/dark-rtl.js"></script>
<script src="js/active.js"></script>
<script src="js/clipboard.min.js"></script>
<script src="js/particles.min.js"></script>
<!-- Function -->
<!-- <script src="js/function.js"></script> -->
<!-- PWA -->
<!-- <script src="js/pwa.js"></script> -->
<script src="js/function.js"></script>

</html>